var BASE_URL = "https://62f8b755e0564480352bf426.mockapi.io/products";



// function cartItem(_Product, _quantity){
//     this.product = _Product;
//     this.quantity = _quantity;
// }

var productList = [];
axios({
    url:`${BASE_URL}`,
    method: "GET",
    })
    .then(function(res){
        let data = res.data;
        for (index = 0; index < data.length; index++){
            productList.push(data[index]);
        }
        renderProducts(productList);
    })
    .catch(function(err){
        console.log(err);
});
// console.log(productList);


// FILTER 

let filter = () => {
    let type = document.getElementById("filter").value;
    axios({
        url:`${BASE_URL}`,
        method:"GET",
    })
    .then(function(res){
        let productList = res.data;
        let filteredProducts = [];
        if (type == null || type == ""){
            filteredProducts = productList;
        } else {
            for(index = 0; index < productList.length; index++){
                if(productList[index].type.toLowerCase()== type){
                    filteredProducts.push(productList[index]);
                }
            }
        }
        renderProducts(filteredProducts);
    })
    .catch(function(err){
        console.log(err);
    })
}
document.getElementById("filter").addEventListener("change",filter);


// ADD TO CART
var cart = [];

let addToCart = (id) => {
    axios({
        url: `${BASE_URL}`,
        method: "GET",
    })
    .then(function(res){
        let newItem = getItem(id, res.data);
        // console.log(newItem);
        pushToCart(newItem);
        saveToLocalStorage(cart);

        // console.log(cart);
        
    })
    .catch(function(err){
        console.log(err);
    })
}

function getItem(id, productList){
    var cartItem = {
        product: {},
        quantity: 1,
        total : 0,
    }
    for (index = 0; index < productList.length; index++){
        if(productList[index].id == id){
            cartItem.product = productList[index];
            cartItem.total = parseInt(productList[index].price) * cartItem.quantity;
        } 
    }
    return cartItem;
}


function pushToCart(cartItem) {
    if (cart.length == 0){
        cart.push(cartItem);
    } else {
        let index = cart.findIndex(item => 
            item.product.id == cartItem.product.id)
            if (index !== -1){
                cart[index].quantity+=1;
                cart[index].total = parseInt(cart[index].product.price) * cart[index].quantity;
            } else {
                cart.push(cartItem);
            }
        }
    cartTotal();
    renderCart(cart);
}


function addQuantity(id){
    // console.log("addQuantity: ",id);
    let index = cart.findIndex(item => item.product.id == id);
    cart[index].quantity +=1;
    cart[index].total = parseInt(cart[index].product.price) * cart[index].quantity;
    cartTotal();
    renderCart(cart);
    saveToLocalStorage(cart);

}
function removeQuantity(id){
    // console.log("removeQuantity: ",id);
    let index = cart.findIndex(item => item.product.id == id);
    if(cart[index].quantity > 1){
        cart[index].quantity -=1;
        cart[index].total = parseInt(cart[index].product.price) * cart[index].quantity;
        cartTotal();
        renderCart(cart);
        saveToLocalStorage(cart);

    } else {
        if(confirm(`Bạn có chắc chắn muốn xoá ${cart[index].product.name} khỏi giỏ hàng?`)){
            cart.splice(cart[index],1); 
            cartTotal();
            renderCart(cart);
            saveToLocalStorage(cart);

        }
        // console.log("Error");
    }
} 

function cartTotal(){
    let cartTotal = 0;
    for (let index = 0; index < cart.length; index++){
        cartTotal += cart[index].total;
    }
    document.getElementById("cart-total").innerHTML = cartTotal;
}

function removeCartItem(id){
    let index = cart.findIndex(item => item.product.id == id);
    if(confirm(`Bạn có chắc chắn muốn xoá ${cart[index].product.name} khỏi giỏ hàng?`)){
        cart.splice(cart[index],1); 
    } else {
        return;
    }
    cartTotal();
    renderCart(cart);
    saveToLocalStorage(cart);
}

function saveToLocalStorage(cart){
    let data = JSON.stringify(cart)
    localStorage.setItem("Cart", data);
}
console.log(cart);

function pay(){
    cart = [];
    console.log(cart);
    document.getElementById("shopping-cart-table").innerHTML = `
    <h5 class="pt-3 text-danger">Không có sản phẩm trong giỏ hàng.</h5>
    `;
    document.getElementById("cart-total").innerHTML = "0";
}

