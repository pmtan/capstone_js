function renderProducts(list){
    let gridHTML = "";
    for (index = 0; index < list.length; index++){
        let gridItem = `
        <div class="card p-3 m-3 col-5 mx-auto">
            <div class="row p-3 d-flex justify-content-between align-items-top ">
                <div class="col-8 d-flex flex-column">
                    <h5 class="text-uppercase">${list[index].name}</h5>
                    <div class="mt-4 d-flex flex-column">
                        <p class="text-capitalize mb-1">+ ${list[index].screen}</p>
                        <p class="text-capitalize mb-1">+ ${list[index].backCamera}</p>
                        <p class="text-capitalize mb-1">+ ${list[index].frontCamera}</p>
                    </div>
                </div>
                <div class="image col-4 p-0">
                    <img src="${list[index].img}" width="160">
                    <div class="price">
                        <span>&#36;${list[index].price}</span>
                    </div>
                </div>
            </div>
            <div class="row px-3">
                <h6 class="text-danger">${list[index].desc}</h6>
            </div>
            <div class="d-flex justify-content-end m-0 p-0">
                <button onclick="addToCart(${list[index].id})" class= "btn btn-danger col-3">Add to cart</button>
            </div>
        </div>
    `;
        gridHTML += gridItem;
    }
    document.getElementById("product-grid").innerHTML = gridHTML;
}

function renderCart(cart){
    let cartHTML = "";
    for (i = 0; i < cart.length; i++){
        let item = `
        <tr>
        <td class="p-4">
          <div class="media align-items-center">
            <img
              src="${cart[i].product.img}"
              class="d-block ui-w-40 ui-bordered mr-4"
              alt=""
            />
            <div class="media-body">
              <a href="#" class="d-block text-dark">${cart[i].product.name}</a>
            </div>
          </div>
        </td>
        <td class="text-right font-weight-semibold align-middle p-4">
        ${cart[i].product.price}
        </td>
        <td class="align-middle p-3">
            <div class="input-group w-75 d-flex">
                <button onclick="removeQuantity(${cart[i].product.id})" class="btn btn-outline-dark">&minus;</button>
                <input id="product-quantity" type="number" min="1" class="form-control" value="${cart[i].quantity}">
                <button onclick="addQuantity(${cart[i].product.id})" class="btn btn-outline-dark">&plus;</button>
            </div>
        </td>
        <td class="text-right font-weight-semibold align-middle p-4">
        ${cart[i].total}
        </td>
        <td class="text-center align-middle px-0">
          <a class="text-danger" onclick="removeCartItem(${cart[i].product.id})">Remove</a>
        </td>
      </tr>
        `
    cartHTML += item;
    }
    document.getElementById("shopping-cart-table").innerHTML = cartHTML;
}
