import { Product } from "./model.js";

export function renderProductList(list){
    let contentHTML = "";
    for (let index = 0; index < list.length; index++){
        let content = `
            <tr>
                <td>${list[index].id}</td>
                <td>${list[index].name}</td>
                <td>${list[index].price}</td>
                <td><img src="${list[index].img}" style="width:20%"></td>
                <td>${list[index].desc}</td>
                <td>
                    <button onclick="showThongTinLenForm(${list[index].id})" data-toggle="modal"
                    data-target="#myModal" class="btn btn-warning">Sửa</button>
                    <button onclick="removeItem(${list[index].id})" class="btn btn-danger">Xoá</button>
                </td>
            </tr>
        `
        contentHTML += content;
    }
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}

export function layThongTinTuForm(){
    let id = document.getElementById("idSP").value;
    let name = document.getElementById("TenSP").value;
    let price = document.getElementById("GiaSP").value;
    let img = document.getElementById("HinhSP").value;
    let desc = document.getElementById("MoTa").value;
    let screen = document.getElementById("ManHinhSP").value;
    let frontCamera = document.getElementById("CameraTruocSP").value;
    let backCamera = document.getElementById("CameraSauSP").value;
    let type = document.getElementById("LoaiSP").value;
    return new Product (id,name,price,screen,backCamera,frontCamera,img,desc,type);
}

export function showThongTin(item) {
    document.getElementById("idSP").value = item.id;
    document.getElementById("idSP").disabled = true;
    document.getElementById("TenSP").value = item.name;
    document.getElementById("GiaSP").value = item.price;
    document.getElementById("ManHinhSP").value = item.screen;
    document.getElementById("CameraTruocSP").value = item.frontCamera;
    document.getElementById("CameraSauSP").value = item.backCamera;
    document.getElementById("HinhSP").value = item.img;
    document.getElementById("MoTa").value = item.desc;
    document.getElementById("LoaiSP").value = item.type;
}

export function resetForm(){
    document.getElementById("idSP").value = "";
    document.getElementById("TenSP").value = "";
    document.getElementById("GiaSP").value = "";
    document.getElementById("HinhSP").value = "";
    document.getElementById("MoTa").value = "";
    document.getElementById("ManHinhSP").value = "";
    document.getElementById("CameraTruocSP").value = "";
    document.getElementById("CameraSauSP").value = "";
    document.getElementById("LoaiSP").value = "";
    document.getElementById("editItemBtn").style.display = "none";
    document.getElementById("addItemBtn").style.display = "block";
    document.getElementById("idSP").disabled = false;
}
window.resetForm = resetForm;

export function validate(valueInput, idError) {
    if (valueInput.trim() == ""){
        document.getElementById(idError).innerText = "Trường không được để trống.";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

export function loadingOn(){
    document.getElementById("loading").style.display = "flex";
}
export function loadingOff(){
    document.getElementById("loading").style.display = "none";
}
