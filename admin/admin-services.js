import { layThongTinTuForm, loadingOff, loadingOn, renderProductList, showThongTin, validate } from "./admin.js";

const BASE_URL = "https://62f8b755e0564480352bf426.mockapi.io/products";

function getData(){
    axios({
        url: `${BASE_URL}`,
        method: "GET",
    })
    .then(function(res){
        // console.log(res.data);
        renderProductList(res.data);
    })
    .catch(function(err){
        console.log(err);
    })
}
getData();


function addItem(){
    let newItem = layThongTinTuForm();
    let isValid = validate(newItem.name,"tb-name") & validate(newItem.price, "tb-price") & validate(newItem.img, "tb-img") & validate(newItem.desc, "tb-desc") & validate(newItem.screen, "tb-screen") & validate(newItem.frontCamera, "tb-front") & validate(newItem.backCamera, "tb-back") & validate(newItem.type, "tb-type");
    if(isValid == false){
        return;
    } else {
        loadingOn();
        axios({
            url: `${BASE_URL}`,
            method: "POST",
            data: newItem,
        })
        .then(function(){
            $('#myModal').modal('hide');
            getData();
            loadingOff();
        })
        .catch(function(err){
            loadingOff();
            console.log(err);
        })
    }
}
window.addItem = addItem;


function removeItem(id){
    loadingOn();
    axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE",
    })
    .then(function(res){
        getData();
        loadingOff();
    })
    .catch(function(err){
        loadingOff();
        console.log(err);
    })
}
window.removeItem = removeItem;


function showThongTinLenForm(id){
    document.getElementById("addItemBtn").style.display = "none";
    document.getElementById("editItemBtn").style.display = "block";
    loadingOn();
    axios({
        url: `${BASE_URL}/${id}`,
        method: "GET",
    })
    .then(function(res){
        let item = res.data;
        let isValid = validate(item.name,"tb-name") & validate(item.price, "tb-price") & validate(item.img, "tb-img") & validate(item.desc, "tb-desc") & validate(item.screen, "tb-screen") & validate(item.frontCamera, "tb-front") & validate(item.backCamera, "tb-back") & validate(item.type, "tb-type");
        if(isValid == false){
            return;
        } else {
        showThongTin(item)
        };
        loadingOff();
    })
    .catch(function(err){
        loadingOff();
        console.log(err);
    })
}
window.showThongTinLenForm = showThongTinLenForm;


function editItem(){
    let editedItem = layThongTinTuForm();
    loadingOn();
    axios({
        url: `${BASE_URL}/${editedItem.id}`,
        method: "PUT",
        data: editedItem,
    })
    .then(function(){
        loadingOff();
        $('#myModal').modal('hide');
        getData();
    })
    .catch(function(err){
        loadingOff();
        console.log(err);
    })
}
window.editItem = editItem;